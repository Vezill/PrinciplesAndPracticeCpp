#pragma once

#include <algorithm>
#include <cmath>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>


namespace util
{

inline void KeepWindowOpen()
{
//        std::cin.clear();
//        std::cout << "Press enter to exit...";
//        std::getchar();
    // Apparently it can be done like this instead
    std::system("pause");  // on unix systems use `pause()` from "unistd.h" instead
}

inline void KeepWindowOpen(const std::string &s)
{
    if (s.empty()) return;
    std::cin.clear();
    std::cin.ignore(120, '\n');

    std::string msg = "Please enter " + s + " to exit\n";
    while (true) {
        std::cout << msg;
        
        std::string ss;
        while (std::cin >> ss && ss != s) std::cout << msg;
        return;
    }
}

inline void SimpleError(const std::string &errMsg)
{
    std::cerr << "Error: " << errMsg << '\n';
    KeepWindowOpen();
    exit(1);
}

inline void Error(const std::string &errMsg)
{
    throw std::runtime_error(errMsg);
}

inline void Error(const std::string &errMsg1, const std::string &errMsg2)
{
    Error(errMsg1 + errMsg2);
}

inline void Error(const std::string &errMsg, int i)
{
    std::ostringstream os;
    os << errMsg << ": " << i;
    Error(os.str());
}

template<class R, class A>
R NarrowCast(const A &a)
{
    R r = R(a);
    if (A(r) != a) Error("info loss");
    return r;
};

inline double Sqrt(int x)
{
    return std::sqrt(double(x));
}

template<typename Out>
inline void StrSplit(const std::string &s, char sep, Out result)
{
    std::stringstream ss;
    ss.str(s);

    std::string item;
    while (std::getline(ss, item, sep)) {
        *(result++) = item;
    }
}

inline std::vector<std::string> StrSplit(const std::string &s, char sep)
{
    std::vector<std::string> ret;
    StrSplit(s, sep, std::back_inserter(ret));
    return ret;
}

inline bool StrIsNumber(const std::string &s)
{
    return !s.empty() && std::all_of(s.begin(), s.end(), ::isdigit);
}

inline bool VStrIsNumber(const std::vector<std::string> &vs)
{
    bool ret = true;
    for (const std::string &s : vs) ret = ret && StrIsNumber(s);
    return ret;
}

}
