/** Try This
 * To see what an uncaught exception error looks like, run a small program that uses `error()`
 * without catching any exceptions.
 */
#include <utilities.hpp>


int main()
{
    util::Error("An Error has occurred!");
}