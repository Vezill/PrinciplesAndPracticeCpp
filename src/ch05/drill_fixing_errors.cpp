/** Drill
 *  Below are 25 code fragments. Each one is meant to be inserted into this "scaffolding".
 *  ```cpp
 *  #include "std_lib_facilities.h"
 *
 *  int main()
 *  {
 *      try {
 *          <<your code here>>
 *          keep_window_open();
 *          return 0;
 *      }
 *      catch (exception &e) {
 *          cerr << "Error: " << e.what() << '\n';
 *          keep_window_open();
 *          return 1;
 *      }
 *      catch (...) {
 *          cerr << "Oops: unknown exception!\n";
 *          keep_window_open();
 *          return 2;
 *      }
 *  }
 *  ```
 *  Each has zero or more errors. Your task is to find and remove all errors in each program. When
 *  you have removed those bugs, the resulting program will compile, run, and write "Success!".
 *  Even if you think you have spotted an error, you still need to enter the (original, unimproved)
 *  program fragment and test ti; you may have guess wrong about what the error is, or there may be
 *  more errors in a fragment than you spotted. Also, one purpose of this drill is to give you a
 *  feel for how your compiler reacts to different kinds of errors. Do not enter the scaffolding 25
 *  times - that's a job for cut and paste or some similar "mechanical" technique. Do not fix
 *  problems by simply deleting a statement; repair them by changing, adding, or deleting a few
 *  characters.
 */
#include <exception>
#include <iostream>
#include <string>
#include <vector>

#include <utilities.hpp>


void code1()
{
    std::cout << "Success!\n";  // 'c' not 'C'
}

void code2()
{
    std::cout << "Success!\n";  // missing a closing '"'
}

void code3()
{
    std::cout << "Success" << "!\n";  // missing a closing and opening '"', also why?
}

void code4()
{
    std::string success = "Success";
    std::cout << success << "!\n";  // "success" was not defined
}

void code5()
{
    int res = 7;  // following the type of v, this should be an 'int'
    std::vector<int> v(10);
    v[5] = res;
    std::cout << "Success!\n";
}

void code6()
{
    std::vector<int> v(10);
    v[5] = 7;  // using circle brackets instead of square brackets
    if (v[5] == 7) std::cout << "Success!\n";  // using circle instead of square brackets, also should be "==" not "!="
}

void code7()
{
    bool cond = true;
    if (cond) std::cout << "Success!\n";  // "cond" is not defined, but needs to be true
    else std::cout << "Fail!\n";
}

void code8()
{
    bool c = true;  // "c" must be true for success
    if (c) std::cout << "Success!\n";
    else std::cout << "Fail!\n";
}

void code9()
{
    std::string s = "ape";
    bool c = "fool" > s;  // "bool" is missing the 'l', also "fool" will always be bigger than "ape", so it should be greater than for success
    if (c) std::cout << "Success!\n";
}

void code10()
{
    std::string s = "ape";
    if (s != "fool") std::cout << "Success!\n";  // s is "ape", so it can't be "fool" for success
}

void code11()
{
    std::string s = "ape";
    if (s != "fool") std::cout << "Success!\n";  // same Error as 10, but also missing a second "<"
}

void code12()
{
    std::string s = "ape";
    if (s + "fool" == "apefool") std::cout << "Success!\n";  // What is this trying to check? string addition doesn't return a bool
}

void code13()
{
    std::vector<char> v(5);
    for (int i = 0; i < v.size(); ++i) ;  // condition was checking a static value
    std::cout << "Success!\n";
}

void code14()
{
    std::vector<char> v(5);
    for (int i = 0; i <= v.size(); ++i) ;  // while it won't cause errors here, you should not go until <= a size call as the final iteration will be past what's in the vector
    std::cout << "Success!\n";
}

void code15()
{
    std::string s = "Success!\n";
    for (int i = 0; i < s.size(); ++i) std::cout << s[i];  // while not a bug, it would be better to use the dynamic size of the string, and in this case we require the full string
}

void code16()
{
    if (true) std::cout << "Success!\n";  // there is no "then" keyword in C++
    else std::cout << "Fail!\n";
}

void code17()
{
    int x = 2000;
    char c = x;
    if (c == char(2000)) std::cout << "Success!\n";  // the int value of '2000' is too big for a char type
}

void code18()
{
    std::string s = "Success!\n";
    for (int i = 0; i < s.size(); ++i) std::cout << s[i];  // s is 9 characters long, the last iteration will print out garbage
}

void code19()
{
    std::vector<int> v(5);  // no type argument given to vector
    for (int i = 0; i <= v.size(); ++i) ;  // last iteration will be past the end of v
    std::cout << "Success!\n";
}

void code20()
{
    int i = 0;
    int j = 9;
    while (i < 10) ++i;  // need to iterate i, not j
    if (j < i) std::cout << "Success!\n";
}

void code21()
{
    int x = 2;
    double d = (5. / (x - 2));
    if (d == 2 * x + 0.5) std::cout << "Success!\n";
}

void code22()
{
    std::string s = "Success!\n";  // you can not use a type argument with string
    for (int i = 0; i <= 10; ++i) std::cout << s[i];
}

void code23()
{
    int i = 0;
    int j = 0;
    while (i < 10) ++j;
    if (j < i) std::cout << "Success!\n";
}

void code24()
{
    int x = 4;
    double d = 5 / (x - 2);
    if (d = 2 * x + 0.5) std::cout << "Success!\n";
}

void code25()
{
    std::cout << "Success!\n";
}

int main()
{
    try {
        std::cout << "Code1:  "; code1();
        std::cout << "Code2:  "; code2();
        std::cout << "Code3:  "; code3();
        std::cout << "Code4:  "; code4();
        std::cout << "Code5:  "; code5();
        std::cout << "Code6:  "; code6();
        std::cout << "Code7:  "; code7();
        std::cout << "Code8:  "; code8();
        std::cout << "Code9:  "; code9();
        std::cout << "Code10:  "; code10();
        std::cout << "Code11:  "; code11();
        std::cout << "Code12:  "; code12();
        std::cout << "Code13:  "; code13();
        std::cout << "Code14:  "; code14();
        std::cout << "Code15:  "; code15();
        std::cout << "Code16:  "; code16();
        std::cout << "Code17:  "; code17();
        std::cout << "Code18:  "; code18();
        std::cout << "Code19:  "; code19();
        std::cout << "Code20:  "; code20();
        std::cout << "Code21:  "; code21();

//        util::KeepWindowOpen();
        return 0;
    }
    catch (std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        util::KeepWindowOpen();
        return 1;
    }
    catch (...) {
        std::cerr << "Oops: Unknown exception!\n";
        util::KeepWindowOpen();
        return 2;
    }
}
