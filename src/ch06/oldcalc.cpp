#include <utilities.hpp>


namespace ch06example
{

class Token
{
public:
    char kind;
    double value;

    Token(char ch)
        : kind(ch), value(0)
    {}
    Token(char ch, double val)
        : kind(ch), value(val)
    {}
};

class TokenStream
{
public:
    TokenStream();

    Token GetNext();
    void PutBack(Token t);

private:
    bool m_Full{false};
    Token m_Buffer;
};

Token TokenStream::GetNext()
{
    if (m_Full) {
        m_Full = false;
        return m_Buffer;
    }

    char ch;
    std::cin >> ch;

    switch (ch) {
        case ';':  // for "print"
        case 'q':  // for "quit"
        case '(':
        case ')':
        case '+':
        case '-':
        case '*':
        case '/':
            return Token {ch};

        case '.':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9': {
            std::cin.putback(ch);
            double val;
            std::cin >> val;
            return Token {'8', val};
        }
        default:
            util::Error("Bad token");
    }
}

void TokenStream::PutBack(Token t)
{
    m_Buffer = t;
    m_Full = true;
}

Token GetToken();
double Primary();
double Expression();
double Term();

int main()
{
    try {
        double val;
        while (std::cin) {
            Token t = ts.GetNext();

            if (t.kind == 'q') break;
            if (t.kind == ';') std::cout << "=" << val << std::endl;
            else ts.PutBack(t);

            val = Expression();
        }
        util::KeepWindowOpen("~0");
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        util::KeepWindowOpen("~1");
        return 1;
    }
    catch (...) {
        std::cerr << "Exception \n";
        util::KeepWindowOpen("~2");
        return 2;
    }
}

Token GetToken()
{
    char ch;
    std::cin >> ch;

    switch (ch) {
        case '(':
        case ')':
        case '+':
        case '-':
        case '*':
        case '/':
            return {ch};
        case '.':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9': {
            std::cin.putback(ch);
            double val;
            std::cin >> val;
            return {'8', val};
        }
        default:
            util::Error("Bad token");
    }
}

double Primary()
{
    Token t = ts.GetNext();
    switch (t.kind) {
        case '(': {
            double d = Expression();
            t = GetToken();
            if (t.kind != ')') util::Error("')' expected");
            return d;
        }
        case '8':
            return t.value;
        default:
            util::Error("Primary expected");
    }
}

double Expression()
{
    double left = Term();
    Token t = ts.GetNext();

    while (true) {
        switch (t.kind) {
            case '+':
                left += Term();
                t = ts.GetNext();
                break;
            case '-':
                left -= Term();
                t = ts.GetNext();
                break;
            default:
                ts.PutBack(t);
                return left;
        }
    }
}

double Term()
{
    double left = Primary();
    Token t = ts.GetNext();

    while (true) {
        switch (t.kind) {
            case '*':
                left *= Primary();
                t = ts.GetNext();
                break;
            case '/': {
                double d = Primary();
                if (d == 0) util::Error("divide by zero");

                left /= d;
                t = ts.GetNext();
                break;
            }
            default:
                ts.PutBack(t);
                return left;
        }
    }
}

}