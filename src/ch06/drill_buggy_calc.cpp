/** Drill
 *  This drill involves a series of modifications to a buggy program to turn it from something
 *  useless into something reasonably useful.
 *  1. Take the calculator from the file `calculator02buggy.cpp`. Get it to compile. You need to
 *     find and fix a few bugs. Those bugs are not in the text in the book. Find the three logic
 *     errors deviously inserted in `calculator02buggy.cpp` and remove them so that the calculator
 *     produces correct results.
 *  2. Change the character used as the exit command from 'q' to 'x'.
 *  3. Change the character used as the print command from ';' to '='.
 *  4. Add a greeting line in `main()`.
 *  5. Improve that greeting by mentioning which operators are available and how to print and exit.
 */
#include <utilities.hpp>


namespace ch06
{

class Token
{
public:
    char kind;      // What kind of token
    double value;   // For numbers: a value

    Token(char ch)
        : kind(ch), value(0)
    {}
    Token(char ch, double val)
        : kind(ch), value(val)
    {}
};

class TokenStream
{
public:
    TokenStream()
        : m_Full(false), m_Buffer(0)
    {}

    Token GetNext();
    void PutBack(Token t);

private:
    bool m_Full;        // Is the stream full?
    Token m_Buffer;     // Where the tokens are stored
};

Token TokenStream::GetNext()
{
    if (m_Full) {
        m_Full = false;
        return m_Buffer;
    }

    char ch;
    std::cin >> ch;

    switch (ch) {
        case '=':   // For 'print'
        case 'x':   // For 'quit'
        case '(':
        case ')':
        case '+':
        case '-':
        case '*':
        case '/':
            return Token(ch);

        case '.':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9': {
            std::cin.putback(ch);

            double val;
            std::cin >> val;
            return Token('8', val);
        }

        default:
            util::Error("Bad token");
    }
}

void TokenStream::PutBack(Token t)
{
    if (m_Full) util::Error("PutBack() into a full buffer");
    m_Buffer = t;
    m_Full = true;
}

TokenStream ts;

double Primary();
double Expression();
double Term();

int main()
{
    std::cout << "*** Buggy C++ Calculator ***\n\n"
              << "Welcome to the Buggy C++ Calculator! It *should* be working, *this* time.\n"
              << "Enter any sequence of numbers, operators and brackets to do some maths!\n"
              << "Enter '=' to print the results, and 'x' to quit. Enjoy your math experience!\n\n";

    try {
        double val = 0;
        while (std::cin) {
            Token t = ts.GetNext();

            if (t.kind == 'x') break;
            if (t.kind == '=') std::cout << "=" << val << std::endl;
            else ts.PutBack(t);

            val = Expression();
        }

        util::KeepWindowOpen();
        return 0;
    }
    catch (std::exception &ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
        util::KeepWindowOpen();
        return 1;
    }
    catch (...) {
        std::cerr << "Oops: unknown exception!\n";
        util::KeepWindowOpen();
        return 2;
    }
}

double Primary()
{
    Token t = ts.GetNext();
    switch (t.kind) {
        case '(': {
            double d = Expression();
            t = ts.GetNext();
            if (t.kind != ')') util::Error("')' expected");
            return d;
        }

        case '8':
            return t.value;

        default:
            util::Error("primary expected");
    }
}

double Term()
{
    double left = Primary();
    Token t = ts.GetNext();

    while (true) {
        switch (t.kind) {
            case '*':
                left *= Primary();
                t = ts.GetNext();
                break;

            case '/': {
                double d = Primary();
                if (d == 0) util::Error("divide by zero");
                left /= d;
                t = ts.GetNext();
                break;
            }

            default:
                ts.PutBack(t);
                return left;
        }
    }
}

double Expression()
{
    double left = Term();
    Token t = ts.GetNext();

    while (true) {
        switch (t.kind) {
            case '+':
                left += Term();
                t = ts.GetNext();
                break;

            case '-':
                left -= Term();
                t = ts.GetNext();
                break;

            default:
                ts.PutBack(t);
                return left;
        }
    }
}

}