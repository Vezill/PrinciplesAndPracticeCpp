/** Simple Calculator
 *
 *  Based on code by Bjarne Stroustrup in Programming: Principles and Practice Using C++
 *  This program implements a basic expression calculator. Input from cin; output to cout.
 *
 *  The grammer for input is:
 *
 *  Statement:
 *      Statement
 *      Print
 *      Quit
 *      Calculation Statement
 *
 *  Print:
 *      ;
 *
 *  Quit:
 *      q
 *
 *  Statement:
 *      Declaration
 *      Expression
 *
 *  Declaration:
 *      "let" Name "=" Expression
 *
 *  Expression:
 *      Term
 *      Expression + Term
 *      Expression - Term
 *
 *  Term:
 *      Primary
 *      Term * Primary
 *      Term / Primary
 *      Term % Primary
 *
 *  Primary:
 *      Number
 *      ( Expression )
 *      - Primary
 *      + Primary
 *
 *  Number:
 *      floating-point-literal
 *
 *  Input comes from cin through TokenStream called ts.
 */
#include <utilities.hpp>


const char TKN_NUMBER = '8';
const char TKN_NAME = 'a';

const char CMD_PRINT = ';';
const char CMD_QUIT = 'q';
const char CMD_LET = 'L';
const std::string KEY_DECL = "let";

const std::string PMT_INPUT = "> ";
const std::string PMT_RESULT = "= ";


class Token
{
public:
    char kind;         // What kind of token
    double value;      // For numbers: a value
    std::string name;  // For keys, name of command

    Token(char ch) : kind{ch}, value{0} {}
    Token(char ch, double val) : kind{ch}, value{val} {}
    Token(char ch, std::string n) : kind{ch}, name{n} {}
};


class TokenStream
{
public:
    TokenStream() : m_Full(false), m_Buffer(0) {}

    Token GetNext();
    void PutBack(Token t);
    void IgnoreUntil(char c);

private:
    bool m_Full;        // Is the stream full?
    Token m_Buffer;     // Where the tokens are stored
};

Token TokenStream::GetNext()
{
    if (m_Full) {
        m_Full = false;
        return m_Buffer;
    }

    char ch;
    std::cin >> ch;

    switch (ch) {
    case CMD_PRINT:
    case CMD_QUIT:
    case '(':
    case ')':
    case '+':
    case '-':
    case '*':
    case '/':
    case '%':
        return Token(ch);

    case '.':
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9': {
        std::cin.putback(ch);

        double val;
        std::cin >> val;
        return Token(TKN_NUMBER, val);
    }

    default:
        if (isalpha(ch)) {
            std::string s;

            s += ch;
            while (std::cin.get(ch) && (isalpha(ch) || isdigit(ch))) s += ch;

            std::cin.putback(ch);
            if (s == KEY_DECL) return Token {CMD_LET};
            return Token {TKN_NAME, s};
        }

        util::Error("Bad token");
    }
}

void TokenStream::PutBack(Token t)
{
    if (m_Full) util::Error("PutBack() into a full buffer");
    m_Buffer = t;
    m_Full = true;
}

void TokenStream::IgnoreUntil(char c)
{
    // Check the buffer first
    if (m_Full && c == m_Buffer.kind) {
        m_Full = false;
        return;
    }
    m_Full = false;

    // Check the input
    char ch = 0;
    while (std::cin >> ch)
        if (ch == c) return;
}


class Variable
{
public:
    std::string name;
    double value;
};


TokenStream ts;
std::vector<Variable> varTable;

void Calculate();
void CleanUpMess();
double Statement();
double Declaration();
double Expression();
double Term();
double Primary();
double GetVariable(const std::string &);
void SetVariable(const std::string &, double);
bool IsDeclared(std::string);
double DefineName(std::string, double);


int main()
{
    std::cout << "*** Buggy C++ Calculator ***\n\n"
              << "Welcome to the Buggy C++ Calculator! It *should* be working, *this* time.\n"
              << "Enter any sequence of numbers, operators and brackets to do some maths!\n"
              << "Enter ';' to print the results, and 'q' to quit. Enjoy your math experience!\n\n";

    try {
        // Predefined names
        DefineName("pi", 3.1415926535);
        DefineName("e", 2.7182818284);

        Calculate();

        util::KeepWindowOpen();
        return 0;
    }
    catch (std::runtime_error &ex) {
        std::cerr << ex.what() << std::endl;
        util::KeepWindowOpen("~");
        return 1;
    }
    catch (std::exception &ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
        util::KeepWindowOpen();
        return 1;
    }
    catch (...) {
        std::cerr << "Oops: unknown exception!\n";
        util::KeepWindowOpen();
        return 2;
    }
}

void Calculate()
{
    while (std::cin)
    try {
        std::cout << PMT_INPUT;

        Token t = ts.GetNext();
        while (t.kind == CMD_PRINT) t = ts.GetNext();  // First discard all "prints"
        if (t.kind == CMD_QUIT) return;                // Quit

        ts.PutBack(t);
        std::cout << PMT_RESULT << Statement() << std::endl;
    }
    catch (std::exception &ex) {
        std::cerr << ex.what() << std::endl;
        CleanUpMess();
    }
}

void CleanUpMess()
{
    ts.IgnoreUntil(CMD_PRINT);
}

double Statement()
{
    Token t = ts.GetNext();

    switch (t.kind) {
    case CMD_LET:
        return Declaration();

    default:
        ts.PutBack(t);
        return Expression();
    }
}

double Declaration()
{
    Token t = ts.GetNext();
    if (t.kind != TKN_NAME) util::Error("name expected in declaration");

    std::string varName = t.name;
    Token t2 = ts.GetNext();
    if (t2.kind != '=') util::Error(KEY_DECL + " missing in declaration of ", varName);

    double d = Expression();
    DefineName(varName, d);

    return d;
}

double Expression()
{
    double left = Term();
    Token t = ts.GetNext();

    while (true) {
        switch (t.kind) {
        case '+':
            left += Term();
            t = ts.GetNext();
            break;

        case '-':
            left -= Term();
            t = ts.GetNext();
            break;

        default:
            ts.PutBack(t);
            return left;
        }
    }
}

double Term()
{
    double left = Primary();
    Token t = ts.GetNext();

    while (true) {
        switch (t.kind) {
        case '*':
            left *= Primary();
            t = ts.GetNext();
            break;

        case '/': {
            double d = Primary();
            if (d == 0) util::Error("divide by zero");
            left /= d;
            t = ts.GetNext();
            break;
        }

        case '%': {
            double d = Primary();
            if (d == 0) util::Error("%: divide by zero");

            left = std::fmod(left, d);
            t = ts.GetNext();
            break;
        }

        default:
            ts.PutBack(t);
            return left;
        }
    }
}

double Primary()
{
    Token t = ts.GetNext();
    switch (t.kind) {
    case '(': {
        double d = Expression();
        t = ts.GetNext();
        if (t.kind != ')') util::Error("')' expected");
        return d;
    }

    case TKN_NUMBER:
        return t.value;

    case '-':
        return -Primary();

    case '+':
        return Primary();

    default:
        util::Error("primary expected");
    }
}

double GetVariable(const std::string &s)
{
    for (const Variable &v : varTable)
        if (v.name == s) return v.value;

    util::Error("Get: undefined variable ", s);
}

void SetVariable(const std::string &s, double d)
{
    for (Variable &v : varTable) {
        if (v.name == s) {
            v.value = d;
            return;
        }
    }

    util::Error("Set: undefined variable ", s);
}

bool IsDeclared(std::string var)
{
    for (const Variable &v : varTable) {
        if (v.name == var) return true;
    }

    return false;
}

double DefineName(std::string var, double val)
{
    if (IsDeclared(var)) util::Error(var, " declared twice");

    varTable.push_back(Variable {var, val});
    return val;
}
