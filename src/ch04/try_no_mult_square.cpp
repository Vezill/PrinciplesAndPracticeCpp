/** Try This
 * Implement `square()` without using the multiplication operator; that is, do `x*x` by repeated
 * addition (start a variable result at `0` and add `x` to it `x` times). Then run some version of
 * "the first program" using that `square()`.
 */
#include <iostream>


int square(int val);

int main()
{
    std::cout << "Here's a version of the 'first program' doing square without multiplication:\n";
    for (int i = 0; i < 100; ++i) {
        std::cout << "square(" << i << ") = " << square(i) << std::endl;
    }
}

int square(int val)
{
    int ret = 0;
    for (int i = 0; i < val; ++i) ret += val;
    return ret;
}