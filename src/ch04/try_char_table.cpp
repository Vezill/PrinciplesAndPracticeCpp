/** Try This
 * The character `'b'` is `char('a'+1)`, `'c' is char('a'+2)`, etc. Use a loop to write out a table
 * of characters with their corresponding integer values.
 */
#include <iostream>


int main()
{
    int val = 97;
    while (val <= 122) {
        std::cout << char(val) << "\t" << val++ << std::endl;
    }
}