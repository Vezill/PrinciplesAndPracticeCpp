/** Try This
 * Use the example above as a model for a program that converts yen ('y'), kroner ('k'), and pounds
 * ('p') into dollars. If you like realism, you can find conversion rates on the web.
 */
#include <iostream>


constexpr double JPY_PER_CDN = 87.43;
constexpr double DKK_PER_CDN = 5.01;
constexpr double GBP_PER_CDN = 0.62;


int main()
{
    std::cout << "*** Canadian Currency Converter ***\n";

    double curr;
    char unit;
    std::string input;
    while (true) {
        std::cout << "\nPlease enter an amount and either 'y' for yen, 'k' for kroner, or 'p' for "
                  << "pounds, or enter 'q' to quit.\n"
                  << "(ex '5.25y') => ";
        std::cin >> input;

        if (input.find('q') != std::string::npos) {
            break;
        }

        auto index = static_cast<int>(input.find_first_of("ykp"));
        curr = std::strtod(input.c_str(), nullptr);
        unit = input[index];

        if (unit == 'y') {
            std::cout << curr << " yen is worth about " << curr / JPY_PER_CDN << " cdn.\n";
        }
        else if (unit == 'k') {
            std::cout << curr << " kroner is worth about " << curr / DKK_PER_CDN << " cdn.\n";
        }
        else if (unit == 'p') {
            std::cout << curr << " pounds are worth about " << curr / GBP_PER_CDN << " cdn.\n";
        }
        else {
            std::cout << "Unknown unit entered.\n";
        }
    }
}