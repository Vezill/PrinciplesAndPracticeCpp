/** Drill
 *  1. Write a program that consists of a `while`-loop that (each time around the loop) reads in two
 *     `ints` and then prints them. Exit the program when a terminating `'|'` is entered.
 *  2. Change the program to write out `the smaller value is:` followed by the smaller of the numbers
 *     and `the larger value is:` followed by the larger value.
 *  3. Augment the program so that it writes the line `the numbers are equal` (only) if they are
 *     equal.
 *  4. Change the program so that it uses `doubles` instead of `ints`.
 *  5. Change the program so that it writes out `the numbers are almost equal` after writing out
 *     which is the larger and the smaller if the two numbers differ by less than 1.0/100.
 *  6. Now change the body of the loop so that it reads just one double each time around. Define two
 *     variables to keep track of which is the smallest and which is the largest value you have seen
 *     so far. Each time through the loop write out the value entered. If it's the smallest so far,
 *     write `the smallest so far` after the number. If it's the largest so far, write `the largest
 *     so far` after the number.
 *  7. Add a unit to each `double` entered; that is, enter values such as `10cm`, `2.5in`, `5ft`, or
 *     `3.33m`. Accept the four units: `cm, m, in, ft`. Assume conversion factors `1m == 100cm`,
 *     `1in == 2.54cm`, `1ft == 12in`. Read the unit indicator into a string. You may consider `12 m`
 *     (with a space between the number and the unit) equivalent to `12m` (without a space).
 *  8. Reject values without units or with "illegal" representations of units, such as `y, yard,
 *     meter, km, and gallon`.
 *  9. Keep track of the sum of values entered (as well as the smallest and largest) and the number
 *     of values entered. When the loop ends, print the smallest, the largest, the number of values,
 *     and the sum of values. Note that to keep the sum, you have to decide on a unit to use for that
 *     sum; use meters.
 * 10. Keep all the values entered (converted into meters) in a `vector`. At the end, write out
 *     those values.
 * 11. Before writing out the values from the `vector`, sort them (that'll make them come out in
 *     increasing order.
 */
#include <algorithm>
#include <iostream>
#include <vector>

#include <utilities.hpp>


static constexpr const char
    *INPUT_REQ = "\nEnter a distance value (units accepted: cm, m, in, ft) or '|' to quit:\n";

double convert_to_meters(double val, const std::string &unit);

int main()
{
    const std::vector<std::string> valid_units = {"cm", "m", "in", "ft"};

    std::cout << "\n\n*** Frobozz Electric Number Comparator ***\n\n";

    double value;
    std::string unit;
    double min_val = 0.;
    double max_val = 0.;
    double sum = 0;
    std::vector<double> values;

    std::string input;
    bool first = true;

    while (true) {
        // Get input from the user
        std::cout << INPUT_REQ;
        std::getline(std::cin, input);

        // Check to see if they want to quit the program
        if (input.find('|') != std::string::npos) {
            break;
        }

        value = std::stod(input);

        // Find the first letter of one of the unit types
        auto index = input.find_first_of("cmif");
        if (index == std::string::npos) {
            std::cout << "INFRACTION: No valid unit found.\n";
            continue;
        }
        unit = input.substr(index, 2);
        // Double check that the unit entered is valid
        if (std::find(valid_units.begin(), valid_units.end(), unit) == valid_units.end()) {
            std::cout << "INFRACTION: Invalid unit was entered!\n";
            continue;
        }

        // If we haven't crashed by this point, output what was entered!
        std::cout << "You entered: " << value << unit << std::endl;

        // Convert the values into meters
        value = convert_to_meters(value, unit);
        sum += value;
        values.push_back(value);

        // Do some fancy stuff with the inputs
        if (first) {
            min_val = value;
            max_val = value;
            first = false;
        }

        min_val = std::min(min_val, value);
        max_val = std::max(max_val, value);
        if (value == min_val) std::cout << "The smallest value so far! " << value << "m\n";
        if (value == max_val) std::cout << "The largest value so far! " << value << "m\n";

    }

    std::cout << "\n\nYou have finished entering values, here are the results:\n"
              << "Total distance entered: " << sum << "m\tNumber of values: " << values.size()
              << "\nSmallest value: " << min_val << "\tLargest value: " << max_val
              << "\nValues entered (in meters): ";

    std::sort(values.begin(), values.end());
    for (double val : values) std::cout << val << ", ";

    std::cout << "\n\nThank you for using this Frobozz Electric tool. Always remember:\n"
              << "\"Who is the boss? Me! I am the boss of you!  --Mir Yannick, the Grand Inquisitor\n";
}

double cm_to_m(double val)
{
    return val / 100;
}

double in_to_cm(double val)
{
    return val * 2.54;
}

double ft_to_in(double val)
{
    return val * 12;
}

double convert_to_meters(double val, const std::string &unit)
{
    double ret;

    if (unit == "cm") ret = cm_to_m(val);
    else if (unit == "in") ret = cm_to_m(in_to_cm(val));
    else if (unit == "ft") ret = cm_to_m(in_to_cm(ft_to_in(val)));
    else ret = val;

    return ret;
}
