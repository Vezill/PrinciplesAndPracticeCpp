/** Try This
 * Rewrite your currency converter program from the previous **Try This** to use a `switch`
 * statement. Add conversions from yuan and kroner (the last one already said to do this...). Which
 * version of the program is easier to write, understand and modify? Why?
 */
#include <iostream>


constexpr double JPY_PER_CDN = 87.43;
constexpr double DKK_PER_CDN = 5.01;
constexpr double GBP_PER_CDN = 0.62;
constexpr double CNY_PER_CDN = 5.33;


int main()
{
    std::cout << "*** Canadian Currency Converter ***\n";

    double curr;
    char unit;
    std::string input;
    while (true) {
        std::cout << "\nPlease enter an amount and either 'y' for yen, 'k' for kroner, 'p' for "
                  << "pounds, or 'c' for yuan, or enter 'q' to quit.\n"
                  << "(ex '5.25y') => ";
        std::cin >> input;

        if (input.find('q') != std::string::npos) {
            break;
        }

        auto index = static_cast<int>(input.find_first_of("ykpc"));
        curr = std::strtod(input.c_str(), nullptr);
        unit = input[index];

        switch (unit) {
        case 'y':
            std::cout << curr << " yen is worth about " << curr / JPY_PER_CDN << " cdn.\n";
            break;
        case 'k':
            std::cout << curr << " kroner is worth about " << curr / DKK_PER_CDN << " cdn.\n";
            break;
        case 'p':
            std::cout << curr << " pounds is worth about " << curr / GBP_PER_CDN << " cdn.\n";
            break;
        case 'c':
            std::cout << curr << " yuan is worth about " << curr / CNY_PER_CDN << " cdn.\n";
            break;
        default:
            std::cout << "Unknown unit entered.\n";
            break;
        }
    }
}
