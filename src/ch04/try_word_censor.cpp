/** Try This
 * Write a program that "bleeps" our words that you don't like; that is, you read in words using
 * `cin` and print them again on `cout`. If a word is among a few you have defined, you write out
 * `BLEEP` instead of that word. Start with one "disliked word" such as
 * ``` string disliked = "Broccoli"; ```
 * When that works, add a few more.
 */
#include <algorithm>
#include <iostream>
#include <vector>

#include <utilities.hpp>


int main()
{
    std::vector<std::string> disliked_words = {"broccoli", "zork", "magic", "darkness", "pizza"};
    std::cout << "Frobozz Electric Speech Corrector\n\n"
              << "Enter some words to have 'corrected':\n";

    std::vector<std::string> words;
    for (std::string input; std::cin >> input;) words.push_back(input);


    std::cout << "You entered the following words, with our patented Frobozz Improver run over:\n";
    int infraction_count;
    for (std::string w : words) {
        // Convert the word to lowercase to make it easier to compare
        std::transform(w.begin(), w.end(), w.begin(), ::tolower);

        if (std::any_of(disliked_words.begin(), disliked_words.end(),
                        [w](std::string s) { return s == w; })) {
            std::cout << "INFRACTION #" << ++infraction_count << std::endl;
        }
        else std::cout << w << std::endl;
    }

    std::cout << std::endl;
    if (infraction_count == 0) std::cout << "Your dictionary is perfect! Carry on loyal citizen.\n";
    else if (infraction_count < 3) std::cout << "You have been given a warning, watch out.\n";
    else std::cout << "You are being arrested for unlawful knowledge of banned words.\n";

    util::KeepWindowOpen();
}