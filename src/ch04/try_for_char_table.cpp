/** Try This
 * Rewrite the character value example from the previous **Try This** to use a `for`-statement. Then
 * modify your program to also write out a table of the integer values for uppercase letters and
 * digits.
 */
#include <iostream>


int main()
{
    for (int val = 48; val <= 122; ++val) {
        std::cout << char(val) << "\t" << val << std::endl;

        if (val == 57) val = 64;
        if (val == 90) val = 96;
    }
}
