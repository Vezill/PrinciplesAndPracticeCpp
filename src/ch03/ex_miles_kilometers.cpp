/** Exercise
 *  Write a program in C++ that converts miles to kilometers. Your program should have a reasonable
 *  prompt for the user to enter a number of miles. Hint: There are 1.609 kilometers to the mile.
 */
#include <iostream>


void ConvertToKilometers();
void ConvertToMiles();

int main()
{
    char choice;
    const std::string valid_choice = "km";

    std::cout << "\n*** Miles to/from Kilometers Converter ***\n\n"
              << "Currently, 1 mile is roughly equivalent to 1.609 kilometers\n"
              << "What measurement would you like to convert to? (k/m)";
    std::cin >> choice;

    while (valid_choice.find(choice) == std::string::npos) {
        std::cout << "Invalid choice entered, please enter either 'k' or 'm': ";
        std::cin >> choice;
    }

    if (choice == 'k') {
        ConvertToKilometers();
    }
    else if (choice == 'm') {
        ConvertToMiles();
    }

    return 0;
}

void ConvertToKilometers()
{
    double value;

    std::cout << "Enter a value in miles: ";
    std::cin >> value;
    std::cout << "That is about " << value * 1.609 << " km";
}

void ConvertToMiles()
{
    double value;

    std::cout << "Enter a value in kilometers: ";
    std::cin >> value;
    std::cout << "That is about " << value * 0.614 << " miles";
}