#include <iostream>


int main()
{
    std::string name;
    double age;

    std::cout << "Please enter your first name and age:\n";
    std::cin >> name >> age;
    std::cout << "Hello, " << name << " (" << age * 12 << " months old)\n";
}