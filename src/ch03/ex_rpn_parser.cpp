/** Exercise
 * Write a program that takes an operation followed by two operands and outputs the result.
 * For example: `+ 100 3.14` or `* 4 5`. Read the operation into a string called `operation` and
 * use an `if-statement` to figure out which operation the user wants, for example,
 * `if (operation == "+")`. Read the operands into variables of type `double`. Implement this for
 * operations called +,-,*,/, plus, minus, mul, and div with their obvious meanings.
 */
#include <algorithm>
#include <iostream>


int main()
{
    std::string op;
    double val1;
    double val2;

    std::cout << "*** Polish Notation Calculator ***\n\n"
              << "λ ";
    while (std::cin >> op >> val1 >> val2) {
        // Make the operator lower case just to make checking easier
        std::transform(op.begin(), op.end(), op.begin(), ::tolower);

        if (op == "+" || op == "plus") std::cout << "> " << val1 + val2;
        else if (op == "-" || op == "minus") std::cout << "> " << val1 - val2;
        else if (op == "*" || op == "mul") std::cout << "> " << val1 * val2;
        else if (op == "/" || op == "div") std::cout << "> " << val1 / val2;
        else if (op.find('q') != std::string::npos) break;
        else std::cout << "Unknown operator entered.";

        std::cout << "\nλ ";
    }
}