/** Drill
 *  1. This drill is to write a program that produces a simple form letter based on user input.
 *     Begin by typing the code from section 3.1 prompting a user to enter his or her first name and
 *     writing "Hello, `recipient`" where `first_name` is the name entered by the user. Then modify
 *     your code as follows: change the prompt to "Enter the name of the person you want to write
 *     to" and change the output to "Dear `recipient`,". Don't forget the comma.
 *  2. Add an introductory line or two, like "How are you? I am fine. I miss you.". Be sure to
 *     indent the first line. Add a few more lines of your choosing -- it's your letter.
 *  3. Now prompt the user for the name of another friend, and store it in `friend_name`. Add a line
 *     to your letter: "Have you seen `friend_name` lately?".
 *  4. Declare a `char` variable called `friend_sex`. Then use two `if-statements` to write the
 *     following:
 *       > If the friend is male, write "If you see `friend_name` please ask him to call me."
 *       > If the friend is female, write "If you see `friend_name` please ask her to call me."
 *  5. Prompt the user to enter the age of the recipient and assign it to an `int` variable `age`.
 *     Have your program write "I hear you just had a birthday and you are `age` years old.". If
 *     `age` is 0 or less, or 110 or more, call `simple_error("You're kidding!")` using
 *     `simple_error()` from `std_lib_facilities.h`.
 *  6. Add this to your letter:
 *       > If your friend is under 12, write "Next year you will be `age+1`."
 *       > If your friend is 17, write "Next year you will be able to vote."
 *       > If your friend is over 70, write "I hope you are enjoying retirement."
 *  7. Add "Yours sincerely," followed by two blank lines for a signature, followed by your name.
 */
#include <iostream>
#include <sstream>

#include <utilities.hpp>


static constexpr const char *OUT_HEADER = "\n\nLetter so far:\n----------\n";

int main()
{
    std::cout << "Welcome to the magical, interactive letter creator! Just answer a few "
        "questions and I will write your letter for you!\n\n";

    std::string recipient;

    std::cout << "-- Enter the name of the person you want to write to: ";
    std::cin >> recipient;

    std::stringstream ss;
    ss << "Dear " << recipient << ",\n\n"
       << "\tHow is life treating you? Last I heard you were in the process of developing a "
       << "nuclear project to \"suppress\" the UK, has that proceeded as planned?\n\n";

    std::cout << OUT_HEADER << ss.str();

    std::string friend_name;
    char friend_sex;
    std::cout << "-- Enter the name of a friend you both know: ";
    std::cin >> friend_name;
    std::cout << "-- And the friends sex (m/f): ";
    std::cin >> friend_sex;

    ss << "Have you seen " << friend_name << " lately? "
       << (friend_sex == 'm' ? "He" : "She") << " still owes me those "
       << "research documents on polar bears eating babies!\n\n";

    std::cout << OUT_HEADER << ss.str();

    int age = 0;
    std::cout << "-- Enter the age of the recipient: ";
    std::cin >> age;

    ss << "I heard from a random fairy that your birthday was a week ago, and that you're " << age
       << " years old now! ";
    if (age <= 0 || age >= 110) {
        util::SimpleError("You're kidding!");
    }
    else if (age < 12) {
        ss << "So next year you're be a ripe young age (that sounded so wrong) of " << age + 1
           << ".";
    }
    else if (age == 17) {
        ss << "Next year you'll be legal! Woo!";
    }
    else if (age > 70) {
        ss << "Hope your retirement is going well, that island must take a lot of upkeep.";
    }

    ss << "\n\n" << "So hopefully we can meet up sometime in the future and plan our world "
       << "domination, I've got a number of good ideas how we can take Russia out of the game!\n\n"
       << "Yours sincerely,\n\n\nNick Donais\n";

    std::cout << "\nHere's the completed letter, hopefully it works for your situation!"
              << "\n----------\n" << ss.str();

    util::KeepWindowOpen();
    return 0;
}