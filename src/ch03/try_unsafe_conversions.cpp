/** Try This:
 *  Run this program with a variety of inputs. Try small values (eg 2 and 3); try large values
 *  (larger than 127, larger than 1000); try negative values; try 56; try 89; try 128; try
 *  non-integer values (eg 56.9 and 56.2). In addition to showing how conversions from double to int
 *  and conversions from int to char are done on your machine, this program shows you what character
 *  (if any) your machine will print for a given integer value.
 */
#include <iostream>


int main()
{
    double d = 0;

    while (std::cin >> d) {     // Repeat the statements below as long as we type in numbers
        int i = d;              // Try to squeeze a double into an int
        char c = i;             // Try to squeeze an int into a char
        int i2 = c;             // Get the integer value of the char

        std::cout << "d == " << d               // The original double
                  << " | i == " << i               // Converted to an int
                  << " | i2 == " << i2             // Int value of the char
                  << " | char(" << c << ")\n";    // The char
    }
}