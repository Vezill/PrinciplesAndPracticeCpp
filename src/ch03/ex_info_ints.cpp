/** Exercise
 *  Write a program that prompts the user to enter two integer values. Store these values in `int`
 *  variables named `val1` and `val2`. Write your program to determine the smaller, larger, sum,
 *  difference, product, and ratio of these values and report them to the user.
 */
#include <iostream>


int main()
{
    int val1;
    int val2;

    std::cout << "Enter two integer numbers and I'll tell you some facts about them: ";
    std::cin >> val1 >> val2;

    int diff;
    int min;
    int max;
    diff = val1 - val2;
    if (diff < 0) {
        std::cout << val2 << " is larger than " << val1 << "\n";
        diff = val2 - val1;
    }
    else if (diff > 0) {
        std::cout << val1 << " is larger than " << val2 << "\n";
    }
    else {
        std::cout << "The numbers are the same!\n";
    }

    std::cout << "Sum: " << val1 + val2 << "\n";
    std::cout << "Difference: " << diff << "\n";
    std::cout << "Product: " << val1 * val2 << "\n";
    std::cout << "Ratio: " << val1 / (double) val2 << "\n";
}