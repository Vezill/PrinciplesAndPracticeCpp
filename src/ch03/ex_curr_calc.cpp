/** Excercise
 * Write a program that prompts the user to enter some number of pennies (1-cent coints), nickles
 * (5-cent coints), dimes (10-cent coins), quarters (25-cent coins), half-dollars (50-cent coins),
 * and one-dollar coins (100-cent coins). Query the user separately for the number of each size
 * coin, eg: `How many pennies do you have?`. Then your program should print out something like
 * this:
 *      You have 23 pennies.
 *      You have 17 nickles.
 *      You have 14 dimes.
 *      You have 7 quarters.
 *      You have 3 half dollars.
 *      The value of all your coins is 573 cents.
 * Make some improvements: if only one of a coin is reported, make the output grammatically correct,
 * eg: `14 dimes and 1 dime (not 1 dimes)`. Also, report the sum in dollars and cents, ie $5.73
 * instead of 573 cents.
 */
#include <algorithm>
#include <iostream>


int main()
{
    std::cout << "*** Currency Calculator and Converter ***\n\n"
              << "This program will ask you for the amount of coins you have, and return both the "
              << "value in cents and dollars. We suggest you find a way to turn in your coins as "
              << "pennies have already been phased out.\n\n";

    while (true) {
        std::cout << "Pennies: ";
        int pennies;
        std::cin >> pennies;

        std::cout << "Nickles: ";
        int nickles;
        std::cin >> nickles;

        std::cout << "Dimes: ";
        int dimes;
        std::cin >> dimes;

        std::cout << "Quarters: ";
        int quarters;
        std::cin >> quarters;

        std::cout << "50-Cent Pieces: ";
        int halves;
        std::cin >> halves;

        std::cout << "Loonies: ";
        int loonies;
        std::cin >> loonies;

        std::cout << "Toonies: ";
        int toonies;
        std::cin >> toonies;

        double dollars = pennies * 0.01 + nickles * 0.05 + dimes * 0.10 + quarters * 0.25 +
            halves * 0.50 + loonies * 1.00 + toonies * 2.00;
        int cents = (int) (dollars * 100);

        std::cout << "Here's a recap of what you've entered:\n"
                  << pennies << " Penn" << (pennies == 1 ? "y" : "ies") << std::endl
                  << nickles << " Nickle" << (nickles == 1 ? "" : "s") << std::endl
                  << dimes << " Dime" << (dimes == 1 ? "" : "s") << std::endl
                  << quarters << " Quarter" << (quarters == 1 ? "" : "s") << std::endl
                  << halves << " 50-Cent Piece" << (halves == 1 ? "" : "s") << std::endl
                  << loonies << " Loon" << (loonies == 1 ? "y" : "ies") << std::endl
                  << toonies << " Toon" << (toonies == 1 ? "y" : "ies") << std::endl
                  << "\nYou have a total of " << cents << "¢ or $" << dollars << "!\n"
                  << "Go get it changed into bills already!\n";

        std::string input;
        while (true) {
            std::cout << "Do you want to calculate a different amount? (y/n) ";
            std::cin >> input;

            std::transform(input.begin(), input.end(), input.begin(), ::tolower);
            if (input.find('y') == std::string::npos && input.find('n') == std::string::npos) {
                std::cout << "Unknown option entered.\n";
            }
            else {
                break;
            }
        }

        if (input.find('n') != std::string::npos) break;
    }
}