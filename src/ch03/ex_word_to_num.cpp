/** Exercise
 *  Write a program that converts spelled-out numbers such as "zero" and "two" into digits, such as
 *  "0" and "2". When the user inputs a number, the program should print out the corresponding
 *  digit. Do it for the values 0, 1, 2, 3, 4 and write out "not a number I know" if the user enters
 *  something that doesn't correspond, such as "Stupid computer!".
 */
#include <algorithm>
#include <iostream>


int main()
{
    std::cout << "Enter the name of a number and I will try to convert it for you!\n";

    std::string input;
    while (std::cin >> input) {
        // There is only a method to convert a char to lowercase, so we have to iterate through the
        // whole string one character at a time. `transform` helps do this in one line instead of a
        // loop.
        std::transform(input.begin(), input.end(), input.begin(), ::tolower);

        if (input == "zero") std::cout << "> You entered 0.\n";
        else if (input == "one") std::cout << "> You entered 1.\n";
        else if (input == "two") std::cout << "> You entered 2.\n";
        else if (input == "three") std::cout << "> You entered 3.\n";
        else if (input == "four") std::cout << "> You entered 4.\n";
        else if (input.find('q') != std::string::npos) break;
        else std::cout << "Unknown value entered, are you thinking ok? Try again:\n";
    }
}