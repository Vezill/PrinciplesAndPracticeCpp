cmake_minimum_required(VERSION 3.8)
project(PrinciplesAndPractice)

set(CMAKE_CXX_STANDARD 11)

set(HEADER_FILES
    include/utilities.hpp)

set(SOURCE_FILES
    src/ch07/calc.cpp)

include_directories(include)

add_executable(PrinciplesAndPractice ${HEADER_FILES} ${SOURCE_FILES})